#!/bin/bash
domains="$1"
current_date=$( date +%s )
#dig +noall +answer $domains | while read _ _ _ _ ip; #for multiple ips on same domain
dig +noall +answer $domains | head -n 1 | while read _ _ _ _ ip; #for single ip on same domain
 do
	 #echo -n "$ip";
	 exp_date=$(echo | openssl s_client -showcerts -servername $domains -connect $ip:443 2>/dev/null | openssl x509 -inform pem -noout -enddate | cut -d "=" -f 2);
	 #echo -n $exp_date
	 expiry_date_conv=$( date -d "$exp_date" +%s )
	 expiry_days="$(( ("${expiry_date_conv} - "${current_date}) / (3600 * 24) ))"
	 #echo "$expiry_days days"
	 if [ "$2" = "days" ]; then
		 echo "${expiry_days}";
	 fi
	 if [ "$2" = "date" ]; then
		 echo "${exp_date}";
	 fi
 done

